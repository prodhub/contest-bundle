<?php
namespace ADW\ContestBundle\Repository;

use ADW\ContestBundle\Model\ShareLogInterface;
use ADW\ContestBundle\Model\ShareWorkInterface;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\ORM\EntityManager;

class ShareLogRepository
{
    /** @var EntityManager */
    private $em;
    /** @var  string */
    private $shareLogClass;

    /**
     * ContestService constructor.
     *
     * @param EntityManager $entityManager
     * @param string        $shareLogClass
     */
    public function __construct(EntityManager $entityManager, $shareLogClass)
    {
        $this->em = $entityManager;
        $this->shareLogClass=$shareLogClass;
    }

    public function getLogClass()
    {
        return $this->shareLogClass;
    }

    /**
     * @param $options
     *
     * @return QueryBuilder
     */
    private function queryLogs($options)
    {
        $qbL = $this->em->getRepository($this->shareLogClass)
            ->createQueryBuilder('l');

        if (isset($options['group'])) {
            $qbL->addSelect("CONCAT(l.socialId, '_', l.socialUserId,'_',l.socialPost) AS HIDDEN name")
                ->groupBy('name');
        }

        if (isset($options['actual'])) {
            $qbL->andWhere('l.actual = :actual')
                ->setParameter('actual', $options['actual']);
        }

        if (isset($options['contest'])) {
            $qbL->andWhere('l.contestName = :contestName')
                ->setParameter('contestName', $options['contest']);
        }

        if (isset($options['work'])) {
            $qbL->andWhere('l.workId = :work')
                ->setParameter('work', $options['work']);
        }

        if (isset($options['period'])) {
            $qbL->andWhere('l.actualAt < :period')
                ->setParameter('period', new \DateTime('-'.$options['period']));
        }

        if (isset($options['startAt']) && isset($options['endAt'])) {
            $qbL->andWhere('l.date >= :startAt')
                ->andWhere('l.date <= :endAt')
                ->andWhere('l.actualAt <= :endAt')
                ->setParameter('startAt', $options['startAt'])
                ->setParameter('endAt', $options['endAt']);
        }

        if (isset($options['limit'])) {
            $qbL->setMaxResults(1);
        }

        if (isset($options['order'])) {
            $qbL->orderBy('l.'.$options['order']);
        }

        return $qbL;
    }

    public function getLogs($options = [])
    {
        return $this->queryLogs($options)->getQuery()->getResult();
    }

    public function getLog($options = [])
    {
        $options['limit'] = 1;

        return $this->queryLogs($options)->getQuery()->getOneorNullResult();
    }

    public function getExportLog($contesName, \DateTime $startAt = null, \DateTime $endAt = null)
    {
        $qb = $this->queryLogs([
            'contest' => $contesName,
            'startAt' => $startAt,
            'endAt'   => $endAt,
        ]);
        $qb->select('l.workId', 'l.socialId', 'l.socialUserId', 'l.socialPost', 'l.date', 'l.logIp',
            'l.logUserAgent','l.actual');

        return $qb->getQuery()->getArrayResult();
    }
}
