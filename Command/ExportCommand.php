<?php
namespace ADW\ContestBundle\Command;

use ADW\ContestBundle\Admin\Controller\AdminController;
use ADW\ContestBundle\Entity\BaseShareLog;
use Doctrine\ORM\AbstractQuery;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\LockHandler;

class ExportCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('adw:contest:export')
            ->addOption('cron', null, InputOption::VALUE_NONE,
                'Для крона, считывает данные из файла')
            ->setDescription('Выгрузка статистики по конкурсам');
    }


    protected function getCacheDir()
    {
        return $this->getContainer()->get('kernel')->getCacheDir();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $cron = $input->getOption('cron');
        if (!$cron) {
            //todo realease logic with agrument command
            return;
        }

        $lock = new LockHandler('adw:contest:export');
        if (!$lock->lock()) {
            return;
        }

        $filenameCron = $this->getCacheDir().'/cron/'.AdminController::$exportFilename;
        if (!file_exists($filenameCron)) {
            return;
        }

        $options = json_decode(file_get_contents($filenameCron), true);
        $repo = $this->getContainer()->get('adw_contest.repo_log');
        $startAt = isset($options['start']) ? new \DateTime($options['start']['date']) : null;
        $endAt = isset($options['end']) ? new \DateTime($options['end']['date']) : null;
        $result = $repo->getExportLog($options['contest'], $startAt, $endAt);
        $filename = $this->generateFile($options['contest'], $result);
        $message = \Swift_Message::newInstance();
        $message->setSubject('Выгрузка логов по конкурсу '.$options['contest'])
            ->setTo($options['email'])
            ->setFrom('no-reply@adwatch.ru');
        $message->attach(\Swift_Attachment::fromPath($filename));
        $result = $this->getContainer()->get('mailer')->send($message);
        if ($result) {
            if ($input->getOption('verbose')) {
                $output->writeln('Export send to '. $options['email']);
            }
            $fs = new Filesystem();
            $fs->remove($filenameCron);
        }

    }

    private function generateFile($name,$data)
    {
        $fs = new Filesystem();
        $filename = sprintf("%s/contest/export_contest_%s_%s.csv", $this->getCacheDir(), $name,
            date("YmdHis"));
        if (!file_exists($this->getCacheDir().'/contest/')) {
            $fs->mkdir($this->getCacheDir().'/contest/');
        }
        $stream = fopen($filename, 'w+');
        $fields = array(
            'Id работы',
            'Дата',
            'Статус',
            'Соц сеть',
            'Id пользователя',
            'Id поста',
            'Ip',
            'UserAgent',
        );
        fputcsv($stream, $fields);
        $socials = array_flip(BaseShareLog::$socialsNames);
        foreach ($data as $log) {
            $row = [
                $log['workId'],
                $log['date']->format('d.m.Y H:i:s'),
                $log['actual'] ? '+' : '-',
                $socials[$log['socialId']],
                $log['socialUserId'],
                $log['socialPost'],
                $log['logIp'],
                $log['logUserAgent'],
            ];
            fputcsv($stream, $row);
        }
        fclose($stream);

        chmod($filename, 777);

        return $filename;
    }
}

?>
