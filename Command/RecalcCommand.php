<?php
namespace ADW\ContestBundle\Command;

use ADW\ContestBundle\Model\ShareLogInterface;
use ADW\ContestBundle\Model\ShareWorkInterface;
use Doctrine\ORM\Query\Expr\Join;
use GuzzleHttp\Client;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RecalcCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     * @throws InvalidArgumentException
     */
    protected function configure()
    {
        $this->setName('adw:contest:recalc')
            ->setDescription('Пересчёт голосов')
            ->addArgument('contestName', InputArgument::OPTIONAL, 'Название конкурса из конфига')
            ->addArgument('start', InputArgument::OPTIONAL, 'Дата начала')
            ->addArgument('end', InputArgument::OPTIONAL, 'Дата окончания');
    }

    private function debug($message = ' ')
    {
        if ($this->verbose) {
            $this->output->writeln($message);
        }
    }

    /**
     * {@inheritdoc}
     * @throws InvalidArgumentException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->verbose = $input->getOption('verbose');
        $this->output = $output;
        $contestName = $input->getArgument('contestName');
        $start = $input->getArgument('start');
        $end = $input->getArgument('end');
        $startAt = $start ? new \DateTime($start) : null;
        $endAt = $end ? new \DateTime($end) : null;
        if (!$contestName) {
            $config = $this->getContestService()->getConfigWorks();
            foreach ($config as $name => $configWork) {
                $this->recalcContest($name, $startAt, $endAt);
            }
        } else {
            $this->recalcContest($contestName);
        }
    }

    private function recalcContest($contestName, $startAt, $endAt)
    {
        $this->debug('Start recalc contest: '.$contestName);
        $em = $this->getContainer()->get('doctrine.orm.default_entity_manager');
        $workClass= $this->getContestService()->getWorkClass($contestName);
        $qbWorks = $em->getRepository($workClass)
            ->createQueryBuilder('a')
            ->select('a.id');
        if ($startAt && $endAt) {
            $qbWorks->andWhere('a.createdAt >= :start')
                ->andWhere('a.createdAt <= :end')
                ->setParameter('start', $dateStart)
                ->setParameter('end', $dateEnd);
        }
        $works=$qbWorks->getQuery()->getResult();

        $progress = new ProgressBar($this->output, count($works));

        /** @var ShareWorkInterface $work */
        foreach ($works as $workId) {
            $work= $this->getContestService()->recalcVotesWork($contestName,$workId);
            $progress->advance();
            $this->debug(' - work: '.$work->getId().', total: '.$work->getTotal());
            $em->clear();
        }
        $progress->finish();
        $this->debug();
    }

    /**
     * @return \ADW\ContestBundle\Services\ContestService
     */
    public function getContestService()
    {
       return $this->getContainer()->get('adw_contest.service');
    }
}
