<?php
namespace ADW\ContestBundle\Command;

use ADW\ContestBundle\Model\ShareLogInterface;
use ADW\ContestBundle\Model\ShareWorkInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CheckCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     * @throws InvalidArgumentException
     */
    protected function configure()
    {
        $this->setName('adw:contest:check')
            ->setDescription('Проверка постов')
            ->addArgument('contestName', InputArgument::OPTIONAL, 'Название конкурса из конфига')
            ->addArgument('start', InputArgument::OPTIONAL, 'Дата начала')
            ->addArgument('end', InputArgument::OPTIONAL, 'Дата окончания')
            ->addOption('all', null, InputOption::VALUE_NONE, 'Всех')
            ->addOption('period', null, InputOption::VALUE_OPTIONAL, 'Через сколько считать лог не актуальным (1 month, 1 week, now)', '1 week');
    }

    private function debug($message = ' ')
    {
        if ($this->verbose) {
            $this->output->writeln($message);
        }
    }

    /**
     * {@inheritdoc}
     * @throws InvalidArgumentException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->verbose = $input->getOption('verbose');
        $this->output = $output;
        $contestName = $input->getArgument('contestName');
        $start = $input->getArgument('start');
        $end = $input->getArgument('end');
        $options['startAt'] = $start ? new \DateTime($start) : null;
        $options['endAt'] = $end ? new \DateTime($end) : null;
        $options['all'] = $input->getOption('all');
        $options['actual'] = true;
        $options['period'] = $input->getOption('period');
        $options['order'] = 'actualAt';
        if ($contestName) {
            $this->checkContest($contestName, $options);

            return;
        }

        $this->config = $this->getContestService()->getConfigWorks();
        foreach ($this->config as $name => $configWork) {
            $this->checkContest($name, $options);
        }
    }

    private function checkContest($contestName, $options)
    {
        $this->debug('Check contest: '.$contestName);
        $repo=$this->getContainer()->get('adw_contest.repo_log');
        if ($options['all']) {
            $logs = $repo->getLogs($options);
            foreach ($logs as $log) {
                $this->checkLog($log);
            }
        } else {
            $options['contest'] = $contestName;
            $log = $repo->getLog($options);
            if ($log) {
                $this->checkLog($log);
            }
        }
    }

    /**
     * @param $log
     */
    private function checkLog(ShareLogInterface $log)
    {
        $this->debug('Log id:'.$log->getId().' social: '.$log->getSocialId());
        $result = $this->getContestService()->checkLog($log);
        $this->debug($result ? 'true' : 'false');
    }


    /**
     * @return \ADW\ContestBundle\Services\ContestService
     */
    public function getContestService()
    {
        return $this->getContainer()->get('adw_contest.service');
    }
}
