<?php
namespace ADW\ContestBundle\Services;

use ADW\ContestBundle\Entity\BaseShareLog;
use ADW\ContestBundle\Model\ShareLogInterface;
use ADW\ContestBundle\Model\ShareWorkInterface;
use ADW\ContestBundle\Repository\ShareLogRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DomCrawler\Crawler;
use GuzzleHttp\Client;

class ContestService
{
    /** @var EntityManager */
    private $em;
    /** @var ShareLogRepository */
    private $shareLogRep;
    /** @var array */
    private $configWorks;

    /**
     * ContestService constructor.
     *
     * @param EntityManager $entityManager
     * @param string        $shareLogClass
     * @param array         $configWorks
     */
    public function __construct(EntityManager $entityManager,ShareLogRepository $shareLogRepository, $configWorks)
    {
        $this->em = $entityManager;
        $this->configWorks = $configWorks;
        $this->shareLogRep=$shareLogRepository;
    }

    /**
     * @param $contestName
     *
     * @return string
     */
    public function getWorkClass($contestName)
    {
        return $this->getConfigWorks()[$contestName]['class'];
    }

    /**
     * @return array
     */
    public function getConfigWorks()
    {
        return $this->configWorks;
    }

    /**
     * @return array
     */
    public function getWorksList()
    {
        return array_keys($this->configWorks);
    }

    /**
     * @return string
     */
    public function getLogClass()
    {
        return $this->shareLogRep->getLogClass();
    }

    /**
     * @param ShareWorkInterface $work
     */
    public function recalcVotesWork($contestName,$workId)
    {
        /** @var ShareWorkInterface $work */
        $work = $this->em->getReference($this->getWorkClass($contestName), $workId);
        $logs = $this->shareLogRep->getLogs([
            'contest' => $contestName,
            'actual'  => true,
            'work'    => $workId,
            'group'   => true,
        ]);

        $votes = [];
        /** @var ShareLogInterface $log */
        foreach (BaseShareLog::$socials as $socialId) {
            $work->setCountSocial($socialId, 0);
        }
        foreach ($logs as $log) {
            $work->incCountSocial($log->getSocialId());
        }

        $work->calcTotal();
        $this->em->flush($work);

        return $work;
    }

    /**
     * @param ShareLogInterface $log
     *
     * @throws \Exception
     */
    public function checkLog(ShareLogInterface $log)
    {
        $log->setActualAt(new \DateTime());
        $this->em->flush($log);
        $result = false;
        switch ($log->getSocialId()) {
            case ShareLogInterface::SOCIAL_VK:
                $result = $this->checkVk($log);
                break;
            case ShareLogInterface::SOCIAL_FB:
                $result = $this->checkFb($log);
                break;
            case ShareLogInterface::SOCIAL_OK:
                $result = $this->checkOk($log);
                break;
        }
        if (!$result) {
            $log->setActual(false);
            $this->em->flush();
        }

        return $result;
    }

    /**
     * @param string $method
     * @param string $url
     * @param array  $params
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function request($method, $url, $params = [])
    {
        $client = new Client([
            'timeout'         => 6,
            'connect_timeout' => 3,
            'http_errors'     => false,
        ]);

        return $client->request($method, $url.'?'.http_build_query($params));
    }

    /**
     * @param ShareLogInterface $log
     *
     * @return bool
     */
    private function checkVk(ShareLogInterface $log)
    {
        $url = 'https://api.vk.com/method/';
        $data = [];
        $data['posts'] = sprintf('%s_%s', $log->getSocialUserId(), $log->getSocialPost());
        $data['version'] = '5.62';
        $response = $this->request('GET', $url.'wall.getById', $data);
        $content = json_decode($response->getBody()->getContents(), true);

        return isset($content['response'][0]['attachment']['link']);
    }

    /**
     * @param ShareLogInterface $log
     *
     * @return bool
     */
    private function checkFb(ShareLogInterface $log)
    {

        $fb =  explode('_',$log->getSocialPost());

        $url = 'https://www.facebook.com/' . $fb[0]. '/posts/' . $fb[1];

        $response = $this->request('GET', $url);

        $crawler = new Crawler($response->getBody()->getContents());

        return $response->getStatusCode() === 200 && count($crawler->filter('div.hidden_elem')) > 0;
    }

    /**
     * @param ShareLogInterface $log
     *
     * @return bool
     */
    private function checkOk(ShareLogInterface $log)
    {
        $url = sprintf('https://ok.ru/profile/%s/statuses/%s', $log->getSocialUserId(),
            $log->getSocialPost());
        $response = $this->request('GET', $url);
        $match = sprintf('/cnts_%s/', $log->getSocialPost());
        $content = $response->getBody()->getContents();

        return preg_match_all($match, $content) > 0;
    }
}
