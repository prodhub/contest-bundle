<?php

namespace ADW\ContestBundle\EventListener;

use Sonata\AdminBundle\Event\ConfigureMenuEvent;

/**
 * Class ConfigureAdminMenuListener
 *
 * @author Artur Vesker
 */
class ConfigureAdminMenuListener
{

    /**
     * @param ConfigureMenuEvent $event
     */
    public function configureMenu(ConfigureMenuEvent $event)
    {
        $menu = $event->getMenu();
        $contest=$menu->addChild('Конкурсы');
        
        $contest->addChild('Экспорт', ['route' => 'adw_contest.admin.export']);
    }

}
