<?php
namespace ADW\ContestBundle\EventListener;

use Doctrine\ORM\Event\LoadClassMetadataEventArgs;

class MappingListener
{
    public function __construct($shareClassLog)
    {
        $this->class = $shareClassLog;
    }

    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs)
    {
        $classMetadata = $eventArgs->getClassMetadata();
        if ($classMetadata->getName() === $this->class) {
            $table = $classMetadata->table;
            $index = [
                'columns' => [
                    'contest_name',
                    'work_id',
                    'social_user_id',
                    'social_id',
                ],
            ];
            $indexes = $table['indexes']['search_votes_idx'] = $index;
            $classMetadata->setPrimaryTable($table);
        }
    }
}
