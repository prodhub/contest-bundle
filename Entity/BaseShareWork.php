<?php
namespace ADW\ContestBundle\Entity;

use ADW\ContestBundle\Model\ShareLogInterface;
use ADW\ContestBundle\Model\ShareWorkInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\MappedSuperclass
 * @ORM\HasLifecycleCallbacks
 */
abstract class BaseShareWork implements ShareWorkInterface
{
    /**
     * @var integer
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    protected $countShareVk = 0;
    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    protected $countShareFb = 0;
    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    protected $countShareOk = 0;
    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    protected $total = 0;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected $updatedAt;

    public function __construct()
    {
    }
    
    /**
     * @ORM\PrePersist
     */
    public function setPrePersist()
    {
        $now = new \Datetime();
        $this->createdAt = $now;
        $this->updatedAt = $now;
    }

    /**
     * @ORM\PreUpdate
     */
    public function setPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getCountShareVk()
    {
        return $this->countShareVk;
    }

    public function incCountSocial($socialId)
    {
        switch ($socialId) {
            case ShareLogInterface::SOCIAL_VK;
                $this->countShareVk++;
                break;
            case ShareLogInterface::SOCIAL_FB;
                $this->countShareFb++;
                break;
            case ShareLogInterface::SOCIAL_OK;
                $this->countShareOk++;
                break;
        }
    }

    public function setCountSocial($socialId, $count)
    {
        switch ($socialId) {
            case ShareLogInterface::SOCIAL_VK;
                $this->countShareVk = $count;
                break;
            case ShareLogInterface::SOCIAL_FB;
                $this->countShareFb = $count;
                break;
            case ShareLogInterface::SOCIAL_OK;
                $this->countShareOk = $count;
                break;
        }
    }

    /**
     * @return int
     */
    public function getCountShareFb()
    {
        return $this->countShareFb;
    }

    /**
     * @return int
     */
    public function getCountShareOk()
    {
        return $this->countShareOk;
    }

    /**
     * @return int
     */
    public function getTotal()
    {
        return $this->total;
    }

    public function calcTotal()
    {
        $this->total = $this->getCountShareVk() + $this->getCountShareFb() + $this->getCountShareOk();
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

}
