<?php
namespace ADW\ContestBundle\Entity;

use ADW\ContestBundle\Model\ShareLogInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\MappedSuperclass
 * @ORM\HasLifecycleCallbacks
 * Indexes added to MappingListener
 */
abstract class BaseShareLog implements ShareLogInterface
{
    public static $socials=[
        self::SOCIAL_VK,
        self::SOCIAL_FB,
        self::SOCIAL_OK
    ];

    public static $socialsNames = [
        'vk' => ShareLogInterface::SOCIAL_VK,
        'fb' => ShareLogInterface::SOCIAL_FB,
        'ok' => ShareLogInterface::SOCIAL_OK,
    ];

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $contestName;
    /**
     * @var int
     * @ORM\Column(type="smallint")
     */
    protected $socialId;
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $socialUserId;
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $socialPost;
    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    protected $workId;
    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected $date;
    /**
     * @var string
     * @ORM\Column(type="text")
     */
    protected $logUserAgent;
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $logIp;
    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    protected $actual = true;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected $actualAt;

    /**
     * @ORM\PrePersist
     */
    public function setPrePersist()
    {
        $this->date = new \Datetime();
        $this->actualAt = clone $this->date;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getContestName()
    {
        return $this->contestName;
    }

    /**
     * @param string $promo
     *
     * @return $this
     */
    public function setContestName($name)
    {
        $this->contestName = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getSocialId()
    {
        return $this->socialId;
    }

    /**
     * @param string $socialId
     *
     * @return $this
     *
     */
    public function setSocialId($socialId)
    {
        $this->socialId = $socialId;

        return $this;
    }

    /**
     * @return string
     */
    public function getSocialUserId()
    {
        return $this->socialUserId;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setSocialUserId($id)
    {
        $this->socialUserId = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getSocialPost()
    {
        return $this->socialPost;
    }

    /**
     * @param string $socialPostId
     *
     * @return $this
     */
    public function setSocialPost($socialPostId)
    {
        $this->socialPost = $socialPostId;

        return $this;
    }

    /**
     * @return int
     */
    public function getWorkId()
    {
        return $this->workId;
    }

    /**
     * @param int $workId
     *
     * @return $this
     */
    public function setWorkId($workId)
    {
        $this->workId = $workId;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return string
     */
    public function getLogUserAgent()
    {
        return $this->logUserAgent;
    }

    /**
     * @param string $logUserAgent
     *
     * @return $this
     */
    public function setLogUserAgent($logUserAgent)
    {
        $this->logUserAgent = $logUserAgent;

        return $this;
    }

    /**
     * @return string
     */
    public function getLogIp()
    {
        return $this->logIp;
    }

    /**
     * @param string $logIp
     *
     * @return $this
     */
    public function setLogIp($logIp)
    {
        $this->logIp = $logIp;

        return $this;
    }

    /**
     * @return bool
     */
    public function isActual()
    {
        return $this->actual;
    }

    /**
     *
     * @param $actual
     *
     * @return $this
     */
    public function setActual($actual)
    {
        $this->actual = $actual;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getActualAt()
    {
        return $this->actualAt;
    }

    /**
     * @param \DateTime $actualAt
     */
    public function setActualAt($actualAt)
    {
        $this->actualAt = $actualAt;
    }
}
