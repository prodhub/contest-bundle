<?php

namespace ADW\ContestBundle\Model;

interface ShareWorkInterface
{
    public function getId();

    public function incCountSocial($socialId);
    public function setCountSocial($socialId, $count);
    public function getCountShareVk();
    public function getCountShareFb();
    public function getCountShareOk();

    public function calcTotal();
    public function getTotal();
}
