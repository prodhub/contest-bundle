<?php
namespace ADW\ContestBundle\Model;

interface ShareLogInterface
{
    CONST SOCIAL_VK = 1;
    CONST SOCIAL_FB = 2;
    CONST SOCIAL_OK = 3;


    /**  Get id */
    public function getId();

    /**
     * Set name
     *
     * @param string $name
     *
     * @return $this
     */
    public function setContestName($name);

    /**
     * Get name
     *
     * @return string
     */
    public function getContestName();

    /**
     * @return int
     */
    public function getSocialId();

    /**
     * @param string $id
     *
     * @return $this
     */
    public function setSocialId($id);

    /**
     * @return string
     */
    public function getSocialUserId();

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setSocialUserId($id);

    /**
     * @return int
     */
    public function getSocialPost();

    /**
     * @param string $postId
     *
     * @return $this
     */
    public function setSocialPost($postId);

    /**
     * @return int
     */
    public function getWorkId();

    /**
     * @param int $workId
     *
     * @return $this
     */
    public function setWorkId($workId);

    /**
     * @return \DateTime
     */
    public function getDate();

    /**
     * @return string
     */
    public function getLogUserAgent();

    /**
     * @param string $logUserAgent
     *
     * @return $this
     */
    public function setLogUserAgent($logUserAgent);

    /**
     * @return string
     */
    public function getLogIp();

    /**
     * @param string $logIp
     *
     * @return $this
     */
    public function setLogIp($logIp);

    public function isActual();

    public function setActual($actual);

    public function getActualAt();

    public function setActualAt($date);
}
