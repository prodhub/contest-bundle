<?php
namespace ADW\ContestBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ExportForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('contest', 'choice', array(
            'required' => true,
            'label'    => 'Конкурс',
            'choices'  => $options['choices'],
        ))->add('start', 'sonata_type_date_picker', array(
            'required' => false,
            'label'    => 'От',
            'widget'   => 'single_text',
            'format'   => 'dd.MM.yyyy',
        ))->add('end', 'sonata_type_date_picker', array(
            'required' => false,
            'label'    => 'До',
            'widget'   => 'single_text',
            'format'   => 'dd.MM.yyyy',
        ))->add('email', 'email', array(
            'label' => 'E-mail',
        ));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setRequired('choices');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'admin_adw_contest_export';
    }
}
