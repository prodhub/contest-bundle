<?php

namespace ADW\ContestBundle\Admin\Controller;

use ADW\ContestBundle\Form\ExportForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/adw/contest")
 */
class AdminController extends Controller
{
    public static $exportFilename = 'adw_contest_export.json';

    /**
     * @Route("/export/", name="adw_contest.admin.export")
     * @Template("ADWContestBundle::export.html.twig")
     */
    public function editAction(Request $request)
    {
        $contests = $this->get('adw_contest.service')->getWorksList();
        $form = $this->createForm(new ExportForm(), null,
            ['choices' => array_combine($contests, $contests)]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $fs = new Filesystem();
            $dataDir = $this->get('kernel')->getCacheDir().'/cron/';
            if (!$fs->exists($dataDir)) {
                $fs->mkdir($dataDir);
            }
            $filename = $dataDir.'adw_contest_export.json';
            if ($fs->exists($filename)) {
                $form->addError(new FormError('Выгрузка уже поставлена'));
            } else {
                $values = $form->getData();
                $fs->dumpFile($filename, json_encode($values));

                $this->addFlash('success', 'Выгрузка поставлена');
            }
        }

        return [
            'form' => $form->createView()
        ];
    }

}
