<?php
namespace ADW\ContestBundle\Controller;

use ADW\ContestBundle\Entity\BaseShareLog;
use ADW\ContestBundle\Model\ShareLogInterface;
use ADW\ContestBundle\Model\ShareWorkInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Csrf\CsrfToken;

/**
 * Each entity controller must extends this class.
 *
 * @abstract
 */
abstract class ShareController extends Controller
{
    /**
     * For proplan project use ailove_promo_system_name
     *
     * @return string
     */
    abstract protected function getContestName();

    CONST CSRF_TOKEN_ID = 'adw_contest_share';

    public function getConfigWorks()
    {
        return $this->getParameter('adw_contest.works');
    }

    public function getClassShareLog()
    {
        return $this->getParameter('adw_contest.share_log_class');
    }

    private function getRequestData($content)
    {
        $body = json_decode($content, true);
        if (!isset($body['data'])) {
            throw new NotFoundHttpException();
        }

        $data = $body['data'];
        $checkData = isset($data['userId']) && isset($data['userId']) && isset($data['postId']) && isset($data['workId']) && isset($data['networkType']) && isset(BaseShareLog::$socialsNames[$data['networkType']]);
        if (!$checkData) {
            throw new NotFoundHttpException();
        }

        return $data;
    }



    /**
     * @Route("/share/", name="adw.contest_share", options={"expose"=true})
     */
    public function shareAction(Request $request)
    {        
        if ($request->getMethod() !== 'POST') {
            throw new NotFoundHttpException();
        }
        $data = $this->getRequestData($request->getContent());

        $csrf = $this->get('security.csrf.token_manager');
        $token = new CsrfToken(static::CSRF_TOKEN_ID, $data['token']);
        if (!$csrf->isTokenValid($token)) {
            throw new BadRequestHttpException();
        }
        $type = $data['networkType'];
        $userId = $data['userId'];
        $postId = $data['postId'];
        $workId = (int)$data['workId'];
        $socialId = BaseShareLog::$socialsNames[$type];
        $status = false;

        $em = $this->get('doctrine.orm.entity_manager');
        $contestName=$this->getContestName();
        $class = $this->getClassShareLog();
        $qb= $em->getRepository($class)->createQueryBuilder('l')
            ->select('COUNT(l.id)');

        $qb->where(
            $qb->expr()->eq('l.contestName', ':contestName'),
            $qb->expr()->eq('l.workId', ':workId'),
            $qb->expr()->eq('l.socialUserId', ':userId'),
            $qb->expr()->eq('l.socialId', ':socialId'),
            $qb->expr()->eq('l.actual','1')
        );
        $qb->setParameters([
            'contestName' => $contestName,
            'workId'      => $workId,
            'userId'      => $userId,
            'socialId'    => $socialId,
        ]);
        $result = (int)$qb->getQuery()->getSingleScalarResult() === 0;
        //TODO move to queue? save log, add vote
        //START SAVE LOG
        /** @var ShareLogInterface $log */
        $log = new $class;
        $log->setContestName($contestName)
            ->setSocialId($socialId)
            ->setSocialUserId($userId)
            ->setSocialPost($postId)
            ->setWorkId($workId)
            ->setLogUserAgent($request->headers->get('User-Agent', 'empty'))
            ->setLogIp($request->getClientIp());
        $em->persist($log);
        $em->flush();
        //END SAVE LOG

        if($result) {
            $workClass=$this->getConfigWorks()[$this->getContestName()]['class'];
            /** @var ShareWorkInterface $work */
            $work = $em->getRepository($workClass)->find($workId);
            if ($work) {
                $work->incCountSocial($socialId);
                $work->calcTotal();
                $em->flush($work);
            }
        }

        return new JsonResponse(['status' => $result]);
    }
}
