#ADW Contest Bundle

### Текущий функционал:
-  Абстракция entity (работа) - за что голосуем
-  Route для сохранение лога голосов + увеличение голоса для работы
-  Команда пересчёта голосов
-  Команда проверки постов в соц.сетях
-  Экcпорт логов из админки на email

## Подключение
- Добавить с composer.json
````
"repositories": [
  { "type": "vcs", "url": "https://bitbucket.org/prodhub/contest-bundle.git" },
]        
````
- Вызвать
````
//composer require adw/contest 0.1.*
composer require adw/contest-bundle 'v0.1'
````

- Добавить в AppKernel
````
new ADW\ContestBundle\ADWContestBundle(),
````

- Создать\изменить entity работы
````
// src/AppBundle/Entity/Vars.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ADW\ContestBundle\Entity\BaseShareWork;

/**
 * @ORM\Entity()
 */
class Work extends BaseShareWork
{
}
````
- Создать entity для лога
````
<?php
namespace AppBundle\Entity;

use ADW\ContestBundle\Entity\BaseShareLog;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity()
 */
class ShareLog extends BaseShareLog
{
}


````
- Добавить в config.yml

Для проектов proplan используем системное название промо.
````
adw_contest:
  share_log_class: 'AppBundle\Entity\ShareLog'
  works:
    SM:
      name: "%ailove_partner_promo%"
      class: 'AppBundle\Entity\Work'
````

- добавить в routing.yml
````
adw_contest_admin:
    resource: "@ADWContestBundle/Admin/Controller"
    type:     annotation
````


 
- Обновить схему бд

## Использование
````
namespace AppBundle\Controller;

use ADW\ContestBundle\Controller\ShareController;

class ApiController extends ShareController
{
    protected function getContestName()
    {
        return $this->getParameter('partner_promo');
    }
    
    ...
````

#### Generate csrf token 
````
use ADW\ContestBundle\Controller\ShareController;

$csrf = $this->get('security.csrf.token_manager');
$token = $csrf->refreshToken(ShareController::CSRF_TOKEN_ID);
````


#### Команды для добавления в крон
Пересчёт голосов:
````
adw:contest:recalc [<contestName>] [<start>] [<end>]
````
Проверка постов в соц.сетях
````
adw:contest:check [<contestName>] [<start>] [<end>] [--all] [--period '1 week']
````
Экспорт логов на email
````
adw:contest:export --cron
````

где 

* contestName - название конкурса из конфига
* start и end - даты для фильтрации работ
* all - проверка сразу всех постов (по умолчанию берётся только 1)
* period - за какой период перепроверять голоса (подставляется в new \DateTime('-1 week'))

В проектах может быть разная логика конкурсов, и лучше создавать свои команды (чтобы не учитывать ненужные работы), например так:

* конкурс, где имеются несколько этапов

````
    protected function configure()
    {
        $this->setName('app:contest:share-check')
            ->setDescription('Проверка постов');
    }

    /**
     * {@inheritdoc}
     * @throws InvalidArgumentException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $stage = $this->getContainer()->get('app.service')->getStageCurrent();
        if (!$stage) {
            return;
        }
        $commandName = 'adw:contest:check';
        $command = $this->getApplication()->find($commandName);
        $arguments = array(
            'command'     => $commandName,
            'contestName' => $this->getContainer()->getParameter('ailove_partner_promo'),
            'start'       => $stage->getStartAt()->format('Y-m-d'),
            'end'         => $stage->getStopAt()->format('Y-m-d'),
            '--period'    => '1 week',
            '--verbose'   => $input->getOption('verbose'),
        );
        $greetInput = new ArrayInput($arguments);
        $command->run($greetInput, $output);
    }
````
